import 'package:flutter/material.dart';
import 'package:front_mobile/screens/form_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Requisição',
      theme: ThemeData(
        primarySwatch: Colors.cyan,
      ),
      home: const FormScreen(),
    );
  }
}


