import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class FormScreen extends StatefulWidget {
  const FormScreen({Key? key}) : super(key: key);

  @override
  State<FormScreen> createState() => _FormScreenState();
}

class _FormScreenState extends State<FormScreen> {
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController referenceController = TextEditingController();

  final dropValueNeighborhood = ValueNotifier<String>('');
  List<String> dropOptionsNeighborhood = [];

  final dropValueCity = ValueNotifier<String>('');
  List<String> dropOptionsCity = [];

  final dropValueCategory = ValueNotifier<String>('');
  List<String> dropOptionsCategory = [];

  @override
  void initState() {
    super.initState();
    fetchDropdownOptions();
  }

  void fetchDropdownOptions() async {
    try {
      final categoryResponse = await http.get(Uri.parse('https://127.0.0.1:8000/category'));
      final neighborhoodResponse = await http.get(Uri.parse('https://127.0.0.1:8000/neighborhood'));
      final cityResponse = await http.get(Uri.parse('https://127.0.0.1:8000/neighborhood'));

      if (categoryResponse.statusCode == 200 &&
          neighborhoodResponse.statusCode == 200 &&
          cityResponse.statusCode == 200) {
        final categoryData = jsonDecode(categoryResponse.body);
        final neighborhoodData = jsonDecode(neighborhoodResponse.body);
        final cityData = jsonDecode(cityResponse.body);

        final categoryOptions = List<String>.from(categoryData['categories']);
        final neighborhoodOptions = List<String>.from(neighborhoodData['neighborhoods']);
        final cityOptions = List<String>.from(cityData['cities']);

        setState(() {
          dropOptionsCategory = categoryOptions;
          dropOptionsNeighborhood = neighborhoodOptions;
          dropOptionsCity = cityOptions;
        });
      } else {
        print('Erro ao obter as opções dos dropdowns');
      }
    } catch (error) {
      print('Erro de conexão: $error');
    }
  }

  Future<void> createRequisition() async {
    final selectedNeighborhood = dropValueNeighborhood.value;
    final selectedCity = dropValueCity.value;
    final selectedCategory = dropValueCategory.value;

    final addressData = {
      'street': streetController.text,
      'reference': referenceController.text,
      'neighborhood_id': selectedNeighborhood,
      'city_id': selectedCity,
    };

    final requestData = {
      'title': titleController.text,
      'description': descriptionController.text,
      'address': addressData,
      'category_id': selectedCategory,
    };

    final url = Uri.parse('https://127.0.0.1:8000/requisition');
    final response = await http.post(url, body: jsonEncode(requestData));

    if (response.statusCode == 201) {
      print('Requisição criada com sucesso');
      // Realize ações adicionais ou exiba uma mensagem de sucesso
    } else {
      print('Erro ao criar requisição');
      // Exiba uma mensagem de erro ou realize ações adicionais, se necessário
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Criar Requisição'),
        ),
        body: ListView(
            padding: const EdgeInsets.all(8.0),
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: titleController,
                  decoration: const InputDecoration(
                    labelText: 'Título',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: descriptionController,
                  decoration: const InputDecoration(
                    labelText: 'Descrição',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: streetController,
                  decoration: const InputDecoration(
                    labelText: 'Rua',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  controller: referenceController,
                  decoration: const InputDecoration(
                    labelText: 'Referência',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ValueListenableBuilder<String>(
                  valueListenable: dropValueNeighborhood,
                  builder: (BuildContext context, String value, _) {
                    return SizedBox(
                      width: 280,
                      child: DropdownButtonFormField<String>(
                        isExpanded: true,
                        hint: const Text('Selecione a opção'),
                        decoration: const InputDecoration(
                          fillColor: Colors.white70,
                          labelText: 'Bairro',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                          ),
                        ),
                        value: (value.isEmpty) ? null : value,
                        onChanged: (option) {
                          setState(() {
                            dropValueNeighborhood.value = option.toString();
                          });
                        },
                        items: dropOptionsNeighborhood.map((opn) {
                          return DropdownMenuItem<String>(
                            value: opn,
                            child: Text(opn),
                          );
                        }).toList(),
                      ),
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ValueListenableBuilder<String>(
                  valueListenable: dropValueCity,
                  builder: (BuildContext context, String value, _) {
                    return SizedBox(
                      width: 280,
                      child: DropdownButtonFormField<String>(
                        isExpanded: true,
                        hint: const Text('Selecione a opção'),
                        decoration: const InputDecoration(
                          fillColor: Colors.white70,
                          labelText: 'Cidade',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                          ),
                        ),
                        value: (value.isEmpty) ? null : value,
                        onChanged: (option) {
                          setState(() {
                            dropValueCity.value = option.toString();
                          });
                        },
                        items: dropOptionsCity.map((opn) {
                          return DropdownMenuItem<String>(
                            value: opn,
                            child: Text(opn),
                          );
                        }).toList(),
                      ),
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ValueListenableBuilder<String>(
                  valueListenable: dropValueCategory,
                  builder: (BuildContext context, String value, _) {
                    return SizedBox(
                      width: 280,
                      child: DropdownButtonFormField<String>(
                        isExpanded: true,
                        hint: const Text('Selecione a opção'),
                        decoration: const InputDecoration(
                          fillColor: Colors.white70,
                          labelText: 'Categoria',
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                          ),
                        ),
                        value: (value.isEmpty) ? null : value,
                        onChanged: (option) {
                          setState(() {
                            dropValueCategory.value = option.toString();
                          });
                        },
                        items: dropOptionsCategory.map((opn) {
                          return DropdownMenuItem<String>(
                            value: opn,
                            child: Text(opn),
                          );
                        }).toList(),
                      ),
                    );
                  },
                ),
              ),

              // Botão de envio
              ElevatedButton(
                onPressed: () {
                  // Chame a função para enviar o formulário para a API
                  submitForm();
                },
                child: const Text('Enviar'),
              ),
            ],
        ),
    );
  }

  void submitForm() async {
    // Verifique se todos os campos necessários foram preenchidos
    if (validateForm()) {
      // Crie o objeto de requisição com os dados do formulário
      final requestData = {
        'title': titleController.text,
        'description': descriptionController.text,
        'address': {
          'street': streetController.text,
          'reference': referenceController.text,
          'neighborhood_id': dropValueNeighborhood.value,
          'city_id': dropValueCity.value,
        },
        'category_id': dropValueCategory.value,
      };

      // Faça a solicitação para a API
      final url = Uri.parse('https://127.0.0.1:8000/requisition');
      final response = await http.post(
        url,
        body: jsonEncode(requestData),
        headers: {'Content-Type': 'application/json'},
      );

      if (response.statusCode == 201) {
        // Sucesso: exiba uma mensagem ou realize ações adicionais
        print('Requisição criada com sucesso');
      } else {
        // Erro: exiba uma mensagem ou realize ações adicionais
        print('Erro ao criar requisição');
      }
    }
  }

  bool validateForm() {
    // Implemente a lógica de validação do formulário aqui
    // Verifique se todos os campos obrigatórios foram preenchidos, etc.
    // Retorne true se o formulário for válido, caso contrário, retorne false.
    // Exemplo simples:
    return titleController.text.isNotEmpty &&
        descriptionController.text.isNotEmpty &&
        streetController.text.isNotEmpty &&
        referenceController.text.isNotEmpty &&
        dropValueNeighborhood.value.isNotEmpty &&
        dropValueCity.value.isNotEmpty &&
        dropValueCategory.value.isNotEmpty;
  }
}
