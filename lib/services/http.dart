import 'dart:convert';

import 'package:http/http.dart' as http;

class HttpService {
  static const String baseUrl = 'https://localhost:8000';

  Future<Map<String, dynamic>> getDropdownOptions() async {
    try {
      final categoryResponse = await http.get(Uri.parse('$baseUrl /categorias'));
      final neighborhoodResponse = await http.get(Uri.parse('$baseUrl /bairros'));
      final cityResponse = await http.get(Uri.parse('$baseUrl /cidades'));

      if (categoryResponse.statusCode == 200 &&
          neighborhoodResponse.statusCode == 200 &&
          cityResponse.statusCode == 200) {
        final categoryData = jsonDecode(categoryResponse.body);
        final neighborhoodData = jsonDecode(neighborhoodResponse.body);
        final cityData = jsonDecode(cityResponse.body);

        final categoryOptions = categoryData['categories'].cast<String>();
        final neighborhoodOptions = neighborhoodData['neighborhoods'].cast<String>();
        final cityOptions = cityData['cities'].cast<String>();

        return {
          'categories': categoryOptions,
          'neighborhoods': neighborhoodOptions,
          'cities': cityOptions,
        };
      } else {
        throw Exception('Erro ao obter as opções dos dropdowns');
      }
    } catch (error) {
      throw Exception('Erro de conexão: $error');
    }
  }

  Future<void> submitRequest(Map<String, dynamic> requestData) async {
    try {
      final response = await http.post(
        Uri.parse('$baseUrl/requisicoes'),
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode(requestData),
      );

      if (response.statusCode == 201) {
        print('Requisição enviada com sucesso');
      } else {
        throw Exception('Erro ao enviar a requisição');
      }
    } catch (error) {
      throw Exception('Erro de conexão: $error');
    }
  }
}
